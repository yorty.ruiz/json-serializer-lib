////////////////////////////////////////////////////////////////////////////////
// Interfaces
////////////////////////////////////////////////////////////////////////////////

import {ConfigurationUtil} from "../config";
import {DefaultValues, InputType} from "./enums";


/**
 * Interface to define generic functions, one arg.
 */
export interface IFunction<A, R> {
  apply(arg: A): R;
}

/**
 * Interface to define generic functions, two args.
 */
export interface IBiFunction<A, B, R> {
  apply(arg1: A, arg2?: B): R;
}

////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////
class IdentityFunction<A> implements IFunction<A, A> {

  // This instance of function will be used in multiple context.
  // eslint-disable-next-line
  static readonly INSTANCE: IFunction<any, any> = new IdentityFunction<any>();

  private constructor() {
    /* uninstantiable */
  }

  apply(arg: A): A {
    return arg;
  }

}

////////////////////////////////////////////////////////////////////////////////
// Type Classify
////////////////////////////////////////////////////////////////////////////////


class TypeClassifier implements IFunction<unknown, InputType> {
  public static readonly INSTANCE = new TypeClassifier();

  private constructor() {
    /* uninstantiable */
  }

  apply(arg: unknown): InputType {
    if (arg === null) {
      return InputType.NATIVE;
    }

    // Only arrays.
    if (Array.isArray(arg)) {
      return InputType.ARRAY;
    }

    if (arg instanceof Date) {
      return InputType.NATIVE;
    }

    // Only 'object'.
    if (typeof (arg) === 'object') {
      return InputType.OBJECT;
    }

    // Only for: 'string' | 'number' | 'boolean' | 'undefined'.
    if (/(?:string)|(?:number)|(?:boolean)|(?:undefined)/ig.test(typeof (arg))) {
      return InputType.NATIVE;
    }

    // If  'symbol' | 'function'.
    return InputType.UNKNOWN;
  }
}


////////////////////////////////////////////////////////////////////////////////
// Deep Equal
////////////////////////////////////////////////////////////////////////////////
class DeepEqualsFunction implements IBiFunction<unknown, unknown, boolean> {

  public static readonly INSTANCE = new DeepEqualsFunction();

  private constructor() {
    /* uninstantiable */
  }

  apply(a: unknown, b: unknown): boolean {
    const typeOfA = TypeClassifier.INSTANCE.apply(a);
    const typeOfB = TypeClassifier.INSTANCE.apply(b);
    if (typeOfA !== typeOfB) {
      return false;
    }

    switch (typeOfA) {
      case InputType.NATIVE: return a === b;
      case InputType.ARRAY: return this.deepEqualArray(a as unknown[], b as unknown[]);
      case InputType.OBJECT: return this.deepEqualObject(a as Record<string, unknown>, b as Record<string, unknown>);
      default: return false;
    }
  }

  private deepEqualArray(a: unknown[], b: unknown[]): boolean {
    if (a.length !== b.length) {
      return false;
    }
    for (let i = 0; i < a.length; i++) {
      if (this.apply(a[i], b[i])) {
        continue;
      }
      return false;
    }
    return true;
  }

  private deepEqualObject(a: Record<string, unknown>, b: Record<string, unknown>): boolean {
    if (Object.getOwnPropertyNames(a).length !== Object.getOwnPropertyNames(b).length) {
      return false;
    }

    for (const prop in a) {
      if (prop in b && this.apply(a[prop], b[prop])) {
        continue;
      }
      return false;
    }
    return true;
  }
}

////////////////////////////////////////////////////////////////////////////////
// Default values
////////////////////////////////////////////////////////////////////////////////
class IsDefaultFunction implements IBiFunction<unknown, unknown[] | null, boolean> {

  public static readonly INSTANCE = new IsDefaultFunction();

  private constructor() {
    /* uninstantiable */
  }

  /**
   * Function to check if the input value is defined as default or not.
   * @param val value to check.
   * @param altDefRef alternative default values.
   * @returns true if and only if the input value is defined as default;
   *          otherwise returns false.
   */
  apply(val: unknown, altDefRef: unknown[] | null = null): boolean {
    return ClassifyDefaultFunction.INSTANCE.apply(val, altDefRef) !== DefaultValues.NOT_A_DEFAULT_VALUE;
  }
}

class ClassifyDefaultFunction implements IBiFunction<unknown, unknown[] | null, DefaultValues> {

  public static readonly INSTANCE = new ClassifyDefaultFunction();

  private constructor() {
    /* uninstantiable */
  }

  /**
   * Function to check if the input value is defined as default or not.
   * @param val value to check.
   * @param altDefRef alternative default values.
   * @returns true if and only if the input value is defined as default;
   *          otherwise returns false.
   */
  apply(val: unknown, altDefRef: unknown[] | null = null): DefaultValues {

    if (val === undefined) {
      return DefaultValues.UNDEFINED;
    }

    if (val === null) {
      return DefaultValues.NULL;
    }

    if (Array.isArray(val)) {
      if (altDefRef && altDefRef.length !== 0 && Array.isArray(altDefRef[0]) && DeepEqualsFunction.INSTANCE.apply(val, altDefRef[0])) {
        return DefaultValues.USER_DEFINED;
      }
      return val.length === 0 ? DefaultValues.EMPTY_ARRAY : DefaultValues.NOT_A_DEFAULT_VALUE;
    }

    if (!altDefRef) {
      if (typeof val === 'object') {
        if (Object.getOwnPropertyNames(val).length === 0) {
          return DefaultValues.EMPTY_OBJ;
        }
        return DefaultValues.NOT_A_DEFAULT_VALUE;
      }

      if (typeof val === 'boolean') {
        return val === ConfigurationUtil.DEFAULTS.BOOLEAN ? DefaultValues.BOOLEAN : DefaultValues.NOT_A_DEFAULT_VALUE;
      }

      if (typeof val === 'string') {
        return val === ConfigurationUtil.DEFAULTS.STRING ? DefaultValues.STRING : DefaultValues.NOT_A_DEFAULT_VALUE;
      }

      if (typeof val === 'number') {
        return val === ConfigurationUtil.DEFAULTS.NUMBER ? DefaultValues.NUMBER : DefaultValues.NOT_A_DEFAULT_VALUE;
      }
    }

    if (altDefRef) {
      for (const item of altDefRef) {
        if (DeepEqualsFunction.INSTANCE.apply(item, val)) {
          return DefaultValues.USER_DEFINED;
        }
      }
    }

    return DefaultValues.NOT_A_DEFAULT_VALUE;
  }
}

////////////////////////////////////////////////////////////////////////////////
/**
 * Set of functions.
 */
export const FunctionUtil = {

  /**
   * Identity function.
   */
  IDENTITY: IdentityFunction.INSTANCE,

  /**
   * Function to compare in deep if two objects or two arrays are equals.
   */
  DEEP_EQUALS: DeepEqualsFunction.INSTANCE,

  /**
   * Function to classify the type of the value:
   *    ARRAY
   *    OBJECT
   *    NATIVE
   *    UNKNOWN
   */
  TYPE_CLASSIFIER: TypeClassifier.INSTANCE,

  /**
   * Function to check if the input value is defined as default or not.
   */
  IS_DEFAULT: IsDefaultFunction.INSTANCE,

  /**
   * Function to classify the kind of default value:
   *    USER_DEFINED
   *    NOT_A_DEFAULT_VALUE
   *    UNDEFINED
   *    ZERO
   *    FALSE
   *    EMPTY_STR
   *    NULL
   *    EMPTY_OBJ
   *    EMPTY_ARRAY
   */
  DEFAULT_VALUE_CLASSIFIER: ClassifyDefaultFunction.INSTANCE

}
