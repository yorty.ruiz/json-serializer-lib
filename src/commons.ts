/**
 * Interface to define object builders.
 * @param <T> any type.
 */
export interface IBuilder<T> {
  /**
   * Builds the final object.
   *
   * @returns an entire build object.
   */
  build(): T;
}

/**
 * To define some attributes for stats.
 */
export interface Stats {
  /**
   * Gets the size in bytes of the input.
   */
  get inputSize(): number;
  /**
   * Gets the size in bytes of the output.
   */
  get outputSize(): number;
  /**
   * Gets the compression ratio (output / input).
   */
  get ratio(): number;
  /**
   * Gets the process runtime in ms.
   */
  get runtime(): number;
}
////////////////////////////////////////////////////////////////////////////////
// OUTPUT
////////////////////////////////////////////////////////////////////////////////
/**
 * Interface to define the output.
 * @param <T> any type.
 */
export interface IOutput<T> {

  /**
   * Gets the stats.
   * @returns an instance of {@link Stats}.
   */
  get stats(): Stats | undefined;

  /**
   * Gets the result.
   * @returns whatever the outcome.
   */
  get result(): T;
}

/**
 * Interface to define the output (when corresponds to collapsed/serialized outcome).
 * @param <T> any type.
 */
export interface ISerializedOutput<T> extends IOutput<T> {
  /**
   * Gets the collapsed/serialized JSON.
   */
  get serializedJSON(): T;
}

/**
 * Interface to define the output (when corresponds to expanded/deserialized outcome).
 * @param <T> any type.
 */
export interface IDeserializedOutput<T> extends IOutput<T> {
  /**
   * Gets the expanded/deserialized JSON.
   */
  get deserializedJSON(): T;
}

/**
 * Abstraction of output.
 * @param <T> any type.
 */
abstract class AbstractOutput<T> implements IOutput<T> {
  private _stats: Stats | undefined;
  private _result: T;
  protected constructor(result: T, stats?: Stats) {
    this._result = result;
    this._stats = stats;
  }

  public get stats(): Stats | undefined {
    return this._stats;
  }

  public get result(): T {
    return this._result;
  }

}

/**
 * Representation of a collapsed/serialized output.
 * @param <T> any type.
 */
export class SerializedOutput<T> extends AbstractOutput<T> implements ISerializedOutput<T> {
  public constructor(result: T, stats?: Stats) {
    super(result, stats);
  }

  public get serializedJSON() {
    return this.result;
  }
}

/**
 * Representation of an expanded/deserialized output.
 * @param <T> any type.
 */
export class DeserializedOutput<T> extends AbstractOutput<T> implements IDeserializedOutput<T> {
  public constructor(result: T, stats?: Stats) {
    super(result, stats);
  }

  get deserializedJSON() {
    return this.result;
  }
}

////////////////////////////////////////////////////////////////////////////////
// Input
////////////////////////////////////////////////////////////////////////////////
export interface Input {
  json: unknown;
  schema: unknown;
}

////////////////////////////////////////////////////////////////////////////////
// Print Logs
////////////////////////////////////////////////////////////////////////////////
export enum LogLevel {
  NONE  = 0,
  TRACE = 1,
  DEBUG = 2,
  INFO  = 2 << 1,
  WARN  = 2 << 2,
  ERROR = 2 << 3,
  ALL   = 2 << 4
}

const LogLevelFunction = {
  [LogLevel.TRACE]: console.trace,
  [LogLevel.DEBUG]: console.debug,
  [LogLevel.INFO]: console.info,
  [LogLevel.WARN]: console.warn,
  [LogLevel.ERROR]: console.error,
};

export class Logger {

  private static INSTANCE = new Logger();
  private map: Record<number, (m: string) => void> = {};

  private constructor(logLevel: LogLevel = LogLevel.WARN) {

    switch (logLevel) {
      case LogLevel.DEBUG:
      case LogLevel.TRACE:
      case LogLevel.INFO:
      case LogLevel.WARN:
      case LogLevel.ERROR:
        this.map[logLevel] = LogLevelFunction[logLevel];
      break;
      case LogLevel.ALL: {
        this.map[LogLevel.TRACE] = LogLevelFunction[LogLevel.TRACE];
        this.map[LogLevel.DEBUG] = LogLevelFunction[LogLevel.DEBUG];
        this.map[LogLevel.INFO] = LogLevelFunction[LogLevel.INFO];
        this.map[LogLevel.WARN] = LogLevelFunction[LogLevel.WARN];
        this.map[LogLevel.ERROR] = LogLevelFunction[LogLevel.ERROR];
      }
      break;
      case LogLevel.NONE: 
        // NOTHING TO DO
        break;
      default: {
        const array: number[] = [ 1 ];
        for (let i = 2; i <= logLevel; i = i << 1) {
          i in LogLevelFunction && array.unshift(i);
        }
        let diff = logLevel;
        for (let i = 0; i < array.length; i++) {
          if (diff >= array[i]) {
            diff -= array[i];
            this.map[array[i]] = LogLevelFunction[array[i]];
          }
        }
        if (diff === 1) {
          this.map[LogLevel.TRACE] = LogLevelFunction[LogLevel.TRACE];
          break;
        }
      }
      break;
    }

  }

  public static setLevel(level: LogLevel): void {
    Logger.INSTANCE = new Logger(level);
  }
  
  public static get instance() : Logger {
    return Logger.INSTANCE;
  }

  public trace(message: () => string): void {
    this.log(LogLevel.TRACE, message);
  }

  public debug(message: () => string): void {
    this.log(LogLevel.DEBUG, message); 
  }

  public info(message: () => string) : void {
    this.log(LogLevel.INFO, message);
  }

  public warn(message: () => string) : void {
    this.log(LogLevel.WARN, message);
  }

  public error(message: () => string) : void {
    this.log(LogLevel.ERROR, message);
  }

  public log(level: LogLevel, message: () => string): void {
    if (level in this.map) {
      this.map[level](message());
    }
  }
}
