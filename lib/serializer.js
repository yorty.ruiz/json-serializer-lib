"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Model = void 0;
const commons = __importStar(require("./commons"));
const schema_1 = require("./schema");
const config_1 = require("./config");
const func_1 = require("./func");
const enums_1 = require("./schema/enums");
const commons_1 = require("./commons");
/**
 * enum to define all the actions.
 */
var Action;
(function (Action) {
    Action[Action["EXCLUDE"] = 0] = "EXCLUDE";
    Action[Action["INCLUDE"] = 1] = "INCLUDE";
})(Action || (Action = {}));
class ProcessOutput {
    /**
     * Constructor.
     *
     * @param _result is any value (by default is undefined).
     * @param _action flag to keep or remove the result (by default is INCLUDE).
     * @param _isOk flag to determine if the result needs more processing (false by default) or not (true).
     */
    constructor(_result = undefined, _action = Action.INCLUDE, _isOk = false) {
        this._result = _result;
        this._action = _action;
        this._isOk = _isOk;
    }
    /**
     * Gets the action.
     *
     * @returns the action.
     */
    get action() {
        return this._action;
    }
    /**
     * Gets the action.
     *
     * @returns the action.
     */
    set action(v) {
        this._action = v;
    }
    /**
     * Gets the result.
     *
     * @returns any value.
     */
    get result() {
        return this._result;
    }
    /**
     * Gets if this result is OK.
     *
     * @returns true if and only if the resul don't need more processing; otherwise returns false.
     */
    get isOk() {
        return this._isOk;
    }
}
class AbstractModel {
    /**
     * Gets a proxified function.
     *
     * @param input the input instance of {@link commons.Input}
     * @param config instance of {@link commons.Input}
     * @param func function to proxify.
     *
     * @returns a function.
     */
    createProxyFunction(input, config, func) {
        if (config.includeStats) {
            return () => {
                let runtime = new Date().getTime();
                const result = func(input.json, input.schema, config);
                runtime = new Date().getTime() - runtime;
                const inSize = JSON.stringify(input.json).length;
                const outSize = JSON.stringify(result).length;
                const stats = {
                    inputSize: inSize,
                    outputSize: outSize,
                    ratio: outSize / inSize,
                    runtime: runtime
                };
                return {
                    json: result,
                    stats: stats
                };
            };
        }
        return () => {
            return {
                json: func(input.json, input.schema, config)
            };
        };
    }
    /**
     * Checks if the value is keeped or removed.
     */
    check(value, schema, config) {
        let userDefaultValues = null;
        let keep = false;
        if (schema && (schema.hasDefVal || schema.keepDef)) {
            userDefaultValues = [schema.defVal];
            keep = schema.keepDef;
        }
        let isOk = false;
        let include = false;
        switch (func_1.FunctionUtil.DEFAULT_VALUE_CLASSIFIER.apply(value, userDefaultValues)) {
            case 6 /* EMPTY_ARRAY */:
                include = config.includeEmptyArrays;
                break;
            case 5 /* EMPTY_OBJ */:
                {
                    if (value instanceof Date) {
                        include = true;
                    }
                    else {
                        include = config.includeEmptyObjects;
                    }
                }
                break;
            case 3 /* STRING */:
                include = config.includeDefaultString;
                break;
            case 2 /* BOOLEAN */:
                include = config.includeDefaultBoolean;
                break;
            case 4 /* NULL */:
                {
                    include = config.includeNullValues || keep;
                    isOk = true; // No more post-process.
                }
                break;
            case 0 /* UNDEFINED */:
                {
                    include = config.includeUndefinedValues || keep;
                    isOk = true; // No more post-process.
                }
                break;
            case 1 /* NUMBER */:
                include = config.includeDefaultNumber;
                break;
            case -2 /* USER_DEFINED */:
                {
                    include = config.includeUserDefinedDefaultValues || keep;
                    isOk = true; // No more post-process.
                }
                break;
            default: include = true;
        }
        return new ProcessOutput(value, include ? Action.INCLUDE : Action.EXCLUDE, isOk);
    }
    /**
     * Process native types.
     *
     * @param typeDescriptor the type descriptor (number, string, date, boolean).
     * @param value input value.
     * @param schema schema to process the input.
     * @param config instance of {@link IConfig}.
     */
    native(value, schema, config) {
        const out = this.check(value, schema, config);
        if (out.action === Action.EXCLUDE || out.isOk) {
            return out;
        }
        return this.processNative(value, schema, config);
    }
    /**
     * Process arrays.
     *
     * @param input array.
     * @param schema schema to process the input.
     * @param config instance of {@link IConfig}.
     */
    processArray(input, schema, config) {
        const out = this.check(input, schema, config);
        if (out.action === Action.EXCLUDE || out.isOk) {
            return out;
        }
        const array = [];
        for (const item of input) {
            // Respect the default values
            if (func_1.FunctionUtil.DEFAULT_VALUE_CLASSIFIER.apply(item) !== -1 /* NOT_A_DEFAULT_VALUE */) {
                array.push(item);
                continue;
            }
            let out = new ProcessOutput(null, Action.EXCLUDE);
            const inputType = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(item);
            switch (inputType) {
                case func_1.InputType.ARRAY:
                    out = this.processArray(item, schema, config);
                    break;
                case func_1.InputType.OBJECT:
                    out = this.processObject(item, schema, config);
                    break;
                case func_1.InputType.NATIVE:
                    out = this.native(item, schema, config);
                    break;
                default:
                    commons_1.Logger.instance.warn(() => `${inputType} is not valid to process an array.`);
            }
            if (out.action === Action.INCLUDE) {
                array.push(out.result);
            }
        }
        return new ProcessOutput(array, Action.INCLUDE, false);
    }
    /**
     * Process object.
     *
     * @param obj object.
     * @param schema schema to process the input.
     * @param config instance of {@link IConfig}.
     */
    processObject(obj, schema, config) {
        const out = schema.parent == null ?
            new ProcessOutput(obj, Action.INCLUDE) : // Skip the check.
            this.check(obj, schema, config);
        if (out.action === Action.EXCLUDE || out.isOk) {
            return out;
        }
        const newObj = {};
        const warnings = [];
        for (const property in obj) { // Iterate by all properties.
            const sch = schema.map.get(property);
            if (!sch) {
                // If the property is not defined.
                warnings.push(`Property: ''${property}'' is not defined in the schema.`);
                switch (config.strategyForUnknownProperty) {
                    case config_1.StrategyForUnknownProperty.IGNORE:
                        break;
                    case config_1.StrategyForUnknownProperty.KEEP:
                        {
                            const out1 = this.check(obj[property], null, config);
                            if (out1.action === Action.INCLUDE) {
                                newObj[property] = out1.result;
                            }
                        }
                        break;
                    case config_1.StrategyForUnknownProperty.THROW_ERROR:
                        throw new Error(warnings[warnings.length - 1]);
                    default:
                        commons_1.Logger.instance.warn(() => `Is not a valid strategy.`);
                        break;
                }
                continue;
            }
            const value = obj[property];
            let out = new ProcessOutput(value, Action.EXCLUDE);
            const inputType = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(value);
            switch (inputType) {
                case func_1.InputType.ARRAY:
                    out = this.processArray(value, sch, config);
                    break;
                case func_1.InputType.OBJECT:
                    out = this.processObject(value, sch, config);
                    break;
                case func_1.InputType.NATIVE:
                    out = this.native(value, sch, config);
                    break;
                default:
                    commons_1.Logger.instance.warn(() => `${inputType} is not valid to process an object.`);
                    break;
            }
            if (out.action === Action.INCLUDE) {
                newObj[sch.propNewName] = out.result;
            }
        }
        warnings.forEach(wm => commons_1.Logger.instance.warn(() => wm));
        return this.postProcess(newObj, schema, config);
    }
    /**
     * Process the input object.
     *
     * @param input object or array to process.
     * @param jsonSchema schema to process the input (JSON format).
     * @param config instance of {@link IConfig}.
     *
     * @returns new processed object.
     */
    process(input, jsonSchema, config) {
        if (func_1.FunctionUtil.TYPE_CLASSIFIER.apply(jsonSchema) !== func_1.InputType.OBJECT) {
            return input;
        }
        const schema = schema_1.Schema.getSchema(jsonSchema);
        switch (func_1.FunctionUtil.TYPE_CLASSIFIER.apply(input)) {
            case func_1.InputType.ARRAY:
                return this.processArray(input, schema, config).result;
            case func_1.InputType.OBJECT:
                return this.processObject(input, schema, config).result;
            case func_1.InputType.NATIVE:
                return input;
            default:
                return {};
        }
    }
}
/**
 * Deserializer class.
 */
class Deserializer extends AbstractModel {
    constructor() {
        super();
    }
    /**
     * Process native types.
     *
     * @param value input value.
     * @param schema schema to process the input.
     */
    processNative(value, schema, config) {
        // Get the default function by the type of the native
        // to transform the native value.
        // By default is IDENTITY function.
        let retFunc = schema.typeDesc.toString() in config.converterMap ?
            config.converterMap[schema.typeDesc.toString()] :
            func_1.FunctionUtil.IDENTITY;
        let arg = value;
        retFunc = schema.hasConv && schema.desFn || retFunc;
        if (schema.hasDefVal) {
            arg = value !== undefined ? value : schema.defVal;
        }
        if (value === undefined) {
            return new ProcessOutput(arg, Action.INCLUDE);
        }
        return new ProcessOutput(retFunc.apply(arg), Action.INCLUDE, false);
    }
    postProcess(obj, schema, config) {
        if (schema.typeDesc === enums_1.TypeDescriptor.ARRAY && func_1.FunctionUtil.DEEP_EQUALS.apply({}, obj)) {
            const action = config.includeEmptyArrays ? Action.INCLUDE : Action.EXCLUDE;
            return new ProcessOutput([], action);
        }
        schema.map.forEach((subSchema) => {
            if (subSchema.propNewName in obj) {
                return;
            }
            if (subSchema.hasDefVal) {
                const out = this.check(subSchema.defVal, subSchema, config);
                if (out.action === Action.INCLUDE) {
                    obj[subSchema.propNewName] = out.result;
                }
                return;
            }
            if (Object.prototype.hasOwnProperty.call(subSchema, 'map')) {
                const objPropSch = subSchema;
                const out = this.postProcess({}, objPropSch, config);
                if (out.action === Action.INCLUDE || out.isOk) {
                    obj[subSchema.propNewName] = out.result;
                }
                return;
            }
            const natPropSch = subSchema;
            let defVal = null;
            const dConf = config;
            if (natPropSch.hasDefVal) {
                defVal = natPropSch.defVal;
            }
            else if (!dConf.useNullInsteadOfDefaulValues) {
                defVal = schema_1.Schema.getDefaultValueByType(natPropSch.typeDesc);
            }
            const out = this.check(defVal, natPropSch, config);
            if (out.action === Action.INCLUDE) {
                obj[natPropSch.propNewName] = out.result;
            }
        });
        let action = Action.INCLUDE;
        if (!config.includeEmptyObjects && func_1.FunctionUtil.DEEP_EQUALS.apply({}, obj)) {
            action = Action.EXCLUDE;
        }
        return new ProcessOutput(obj, action);
    }
    apply(input, altConfig) {
        const result = this.createProxyFunction(input, altConfig || config_1.ConfigurationUtil.globalDesConfig, super.process.bind(this))();
        return new commons.DeserializedOutput(result.json, result.stats);
    }
}
class Serializer extends AbstractModel {
    constructor() {
        super();
    }
    /**
     * Process native types.
     *
     * @param value input value.
     * @param schema schema to process the input.
     * @param config instance of {@link IConfig}.
     */
    processNative(value, schema, config) {
        // Get the default function by the type of the native
        // to transform the native value.
        // By default is IDENTITY function.
        let retFunc = schema.typeDesc.toString() in config.converterMap ?
            config.converterMap[schema.typeDesc.toString()] :
            func_1.FunctionUtil.IDENTITY;
        let arg = value;
        retFunc = schema.hasConv && schema.serFn || retFunc;
        let action = Action.INCLUDE;
        if (schema.hasDefVal) { // User defined default value.
            arg = value !== undefined ? value : schema.defVal;
            if (!config.includeDefaultValues && value === schema.defVal) {
                action = schema.keepDef ? Action.INCLUDE : Action.EXCLUDE;
            }
        }
        return new ProcessOutput(retFunc.apply(arg), action, false);
    }
    postProcess(val, schema, config) {
        let action = Action.INCLUDE;
        if (!config.includeEmptyObjects && func_1.FunctionUtil.DEEP_EQUALS.apply({}, val)) {
            action = Action.EXCLUDE;
        }
        return new ProcessOutput(val, action);
    }
    apply(input, altConfig) {
        const result = this.createProxyFunction(input, altConfig || config_1.ConfigurationUtil.globalSerConfig, super.process.bind(this))();
        return new commons.SerializedOutput(result.json, result.stats);
    }
}
class Model {
    static serialize(obj, schema, altConfig) {
        const input = {
            json: obj,
            schema: schema,
        };
        return Model.SERIALIZER_INSTANCE.apply(input, altConfig);
    }
    static deserialize(obj, schema, altConfig) {
        const input = {
            json: obj,
            schema: schema_1.Schema.invert(schema),
        };
        return Model.DESERIALIZER_INSTANCE.apply(input, altConfig);
    }
}
exports.Model = Model;
Model.DESERIALIZER_INSTANCE = new Deserializer();
Model.SERIALIZER_INSTANCE = new Serializer();
