"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SerializationSettingsBuilder = exports.DeserializationSettingsBuilder = void 0;
const configuration_1 = require("./configuration");
const enums_1 = require("./enums");
class SettingsBuilder {
    constructor(_config) {
        this._config = _config;
    }
    get config() {
        return this._config;
    }
    build() {
        return this._config;
    }
    includeStats(answer) {
        this._config.includeStats = answer;
        return this;
    }
    includeDefaultValues(answer) {
        this._config.includeDefaultValues = answer;
        this._config.includeEmptyArrays = answer;
        this._config.includeEmptyObjects = answer;
        this._config.includeDefaultString = answer;
        this._config.includeDefaultBoolean = answer;
        this._config.includeNullValues = answer;
        this._config.includeUndefinedValues = answer;
        this._config.includeDefaultNumber = answer;
        this._config.includeUserDefinedDefaultValues = answer;
        return this;
    }
    includeEmptyArrays(answer) {
        this._config.includeEmptyArrays = answer;
        return this;
    }
    includeNullValues(answer) {
        this._config.includeNullValues = answer;
        return this;
    }
    includeUndefinedValues(answer) {
        this._config.includeUndefinedValues = answer;
        return this;
    }
    includeEmptyObjects(answer) {
        this._config.includeEmptyObjects = answer;
        return this;
    }
    includeDefaultBoolean(answer) {
        this._config.includeDefaultBoolean = answer;
        return this;
    }
    includeDefaultString(answer) {
        this._config.includeDefaultString = answer;
        return this;
    }
    includeDefaultNumber(answer) {
        this._config.includeDefaultNumber = answer;
        return this;
    }
    includeUserDefinedDefaultValues(answer) {
        this._config.includeUserDefinedDefaultValues = answer;
        return this;
    }
    strategyForUnknownProperty(strategy) {
        this._config.strategyForUnknownProperty = strategy;
        return this;
    }
}
class DeserializationSettingsBuilder {
    constructor() {
        this._settingsBuilder = new SettingsBuilder(new configuration_1.DeserializationConfig());
        this._settingsBuilder.includeDefaultValues(true)
            .strategyForUnknownProperty(enums_1.StrategyForUnknownProperty.KEEP)
            .includeStats(false);
    }
    setBooleanConverter(converter) {
        this._settingsBuilder.config.toBooleanFrom = converter;
        return this;
    }
    useNullInsteadOfDefaulValues(answer) {
        this._settingsBuilder.config.useNullInsteadOfDefaulValues = answer;
        return this;
    }
    setDateConverter(converter) {
        this._settingsBuilder.config.toDateFrom = converter;
        return this;
    }
    includeStats(answer) {
        this._settingsBuilder.includeStats(answer);
        return this;
    }
    includeDefaultValues(answer) {
        this._settingsBuilder.includeDefaultValues(answer);
        return this;
    }
    includeEmptyArrays(answer) {
        this._settingsBuilder.includeEmptyArrays(answer);
        return this;
    }
    includeDefaultString(answer) {
        this._settingsBuilder.includeDefaultString(answer);
        return this;
    }
    includeNullValues(answer) {
        this._settingsBuilder.includeNullValues(answer);
        return this;
    }
    includeUndefinedValues(answer) {
        this._settingsBuilder.includeUndefinedValues(answer);
        return this;
    }
    includeEmptyObjects(answer) {
        this._settingsBuilder.includeEmptyObjects(answer);
        return this;
    }
    includeDefaultBoolean(answer) {
        this._settingsBuilder.includeDefaultBoolean(answer);
        return this;
    }
    includeDefaultNumber(answer) {
        this._settingsBuilder.includeDefaultNumber(answer);
        return this;
    }
    includeUserDefinedDefaultValues(answer) {
        this._settingsBuilder.includeUserDefinedDefaultValues(answer);
        return this;
    }
    strategyForUnknownProperty(strategy) {
        this._settingsBuilder.strategyForUnknownProperty(strategy);
        return this;
    }
    build() {
        return this._settingsBuilder.build();
    }
}
exports.DeserializationSettingsBuilder = DeserializationSettingsBuilder;
DeserializationSettingsBuilder.DEFAULT = new DeserializationSettingsBuilder().build();
class SerializationSettingsBuilder {
    constructor() {
        this._settingsBuilder = new SettingsBuilder(new configuration_1.SerializationConfig());
        this._settingsBuilder.includeDefaultValues(false)
            .strategyForUnknownProperty(enums_1.StrategyForUnknownProperty.IGNORE)
            .includeStats(false);
    }
    setBooleanConverter(converter) {
        this._settingsBuilder.config.fromBooleanTo = converter;
        return this;
    }
    setDateConverter(converter) {
        this._settingsBuilder.config.fromDateTo = converter;
        return this;
    }
    includeStats(answer) {
        this._settingsBuilder.includeStats(answer);
        return this;
    }
    includeDefaultValues(answer) {
        this._settingsBuilder.includeDefaultValues(answer);
        return this;
    }
    includeEmptyArrays(answer) {
        this._settingsBuilder.includeEmptyArrays(answer);
        return this;
    }
    includeDefaultString(answer) {
        this._settingsBuilder.includeDefaultString(answer);
        return this;
    }
    includeNullValues(answer) {
        this._settingsBuilder.includeNullValues(answer);
        return this;
    }
    includeUndefinedValues(answer) {
        this._settingsBuilder.includeUndefinedValues(answer);
        return this;
    }
    includeEmptyObjects(answer) {
        this._settingsBuilder.includeEmptyObjects(answer);
        return this;
    }
    includeDefaultBoolean(answer) {
        this._settingsBuilder.includeDefaultBoolean(answer);
        return this;
    }
    includeDefaultNumber(answer) {
        this._settingsBuilder.includeDefaultNumber(answer);
        return this;
    }
    includeUserDefinedDefaultValues(answer) {
        this._settingsBuilder.includeUserDefinedDefaultValues(answer);
        return this;
    }
    strategyForUnknownProperty(strategy) {
        this._settingsBuilder.strategyForUnknownProperty(strategy);
        return this;
    }
    build() {
        return this._settingsBuilder.build();
    }
}
exports.SerializationSettingsBuilder = SerializationSettingsBuilder;
SerializationSettingsBuilder.DEFAULT = new SerializationSettingsBuilder().build();
