"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigurationUtil = exports.StrategyForUnknownProperty = exports.DeserializationConfig = exports.SerializationConfig = void 0;
const settingsBuilder_1 = require("./settingsBuilder");
////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////
var configuration_1 = require("./configuration");
Object.defineProperty(exports, "SerializationConfig", { enumerable: true, get: function () { return configuration_1.SerializationConfig; } });
Object.defineProperty(exports, "DeserializationConfig", { enumerable: true, get: function () { return configuration_1.DeserializationConfig; } });
var enums_1 = require("./enums");
Object.defineProperty(exports, "StrategyForUnknownProperty", { enumerable: true, get: function () { return enums_1.StrategyForUnknownProperty; } });
/**
 * Utility class.
 */
class ConfigurationUtil {
    /**
     * Gets the global configuration for searilation process.
     */
    static get globalSerConfig() {
        return ConfigurationUtil._serConfig;
    }
    /**
     * Overrides the global configuration for searilation process.
     */
    static set globalSerConfig(v) {
        ConfigurationUtil._serConfig = v;
    }
    /**
     * Gets the global configuration for desearilation process.
     */
    static get globalDesConfig() {
        return ConfigurationUtil._desConfig;
    }
    /**
     * Overrides the global configuration for desearilation process.
     */
    static set globalDesConfig(v) {
        ConfigurationUtil._desConfig = v;
    }
    /**
     * Resets the global config to its predefined values.
     */
    static resetGlobalConfig() {
        ConfigurationUtil._desConfig = settingsBuilder_1.DeserializationSettingsBuilder.DEFAULT;
        ConfigurationUtil._serConfig = settingsBuilder_1.SerializationSettingsBuilder.DEFAULT;
    }
    static get settingsBuilderFor() {
        return {
            DESERIALIZER: new settingsBuilder_1.DeserializationSettingsBuilder(),
            SERIALIZER: new settingsBuilder_1.SerializationSettingsBuilder()
        };
    }
}
exports.ConfigurationUtil = ConfigurationUtil;
// Default config.
ConfigurationUtil._desConfig = settingsBuilder_1.DeserializationSettingsBuilder.DEFAULT;
ConfigurationUtil._serConfig = settingsBuilder_1.SerializationSettingsBuilder.DEFAULT;
ConfigurationUtil.DEFAULTS = {
    BOOLEAN: false,
    STRING: '',
    NUMBER: 0,
};
