"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Convert = void 0;
const commons_1 = require("../commons");
class AbstractConverterBasedOnMap {
    constructor(map) {
        this.validate(map);
        this._map = map;
    }
    validate(map) {
        if (!map) {
            throw new Error('"map" must be defined.');
        }
    }
    apply(arg) {
        return this.getValueFromKey(arg);
    }
    /**
     * @returns a bound function of apply function.
     */
    get func() {
        return this.apply.bind(this);
    }
}
class ConvertFromBoolean extends AbstractConverterBasedOnMap {
    constructor(map) {
        super(map);
    }
    validate(map) {
        super.validate(map);
        if (!('true' in map) || !('false' in map)) {
            throw new Error('Property "true" and property "false", both must be defined.');
        }
    }
    getValueFromKey(key) {
        const str = key ? 'true' : 'false';
        return this._map[str];
    }
    static toCustom(map) {
        return new ConvertFromBoolean(map);
    }
}
class ConvertToBoolean extends AbstractConverterBasedOnMap {
    constructor(map) {
        super(map);
    }
    validate(map) {
        super.validate(map);
        let c = 0;
        for (const prop in map) {
            if (map[prop] === true || map[prop] === false) {
                c++;
                continue;
            }
            delete map[prop];
        }
        if (c < 2) {
            throw new Error('The map must contain a least two properties with the values: "true" and "false". Example: {"a": true, "b": false}.');
        }
        if (c > 2) {
            commons_1.Logger.instance.warn(() => 'More than 3 properties are defined.');
        }
    }
    getValueFromKey(key) {
        if (Object.prototype.hasOwnProperty.call(this._map, key)) {
            return this._map[key];
        }
        throw new Error(`There is not property: ${key} defined in _map`);
    }
    static toCustom(map) {
        return new ConvertToBoolean(map);
    }
}
class ConvertFromDate {
    apply(arg) {
        return arg.getTime();
    }
}
class ConvertToDate {
    apply(arg) {
        return new Date(arg);
    }
}
class Convert {
}
exports.Convert = Convert;
Convert.FromBoolean = {
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 1
     *    false to 0
     */
    To_0_Or_1: new ConvertFromBoolean({ 'true': 1, 'false': 0 }),
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 'T'
     *    false to 'F'
     */
    To_F_Or_T: new ConvertFromBoolean({ 'true': 'T', 'false': 'F' }),
    /**
     * Instance of {@link IFunction} that converts:
     *    true to 'Y'
     *    false to 'N'
     */
    To_N_Or_Y: new ConvertFromBoolean({ 'true': 'Y', 'false': 'N' }),
    /**
     * Custom converter.
     *
     * Example:
     * const convToBool = Convert.Boolean.TO_CUSTOM(
     *    {
     *      'true': 'A',
     *      'false': 'B'
     *    }
     * );
     *
     * convToBool.apply(true); // Returns 'A'.
     * convToBool.apply(false); // Returns 'B'.
     */
    ToCustom: ConvertFromBoolean.toCustom
};
Convert.ToBoolean = {
    /**
     * Instance of {@link IFunction} that converts:
     *    1 to true
     *    0 to false
     */
    From_0_Or_1: new ConvertToBoolean({ '1': true, '0': false }),
    /**
     * Instance of {@link IFunction} that converts:
     *    'T' to true
     *    'F' to false
     */
    From_F_Or_T: new ConvertToBoolean({ 'T': true, 'F': false }),
    /**
     * Instance of {@link IFunction} that converts:
     *    'Y' to true
     *    'N' to false
     */
    From_N_Or_Y: new ConvertToBoolean({ 'Y': true, 'N': false }),
    /**
     * Custom converter.
     *
     * Example:
     * const convToBool = Convert.Boolean.FROM_CUSTOM(
     *    {
     *      'A': true,
     *      'B': false
     *    }
     * );
     *
     * convToBool.apply('A'); // Returns true.
     * convToBool.apply('B'); // Returns false.
     */
    FromCustom: ConvertToBoolean.toCustom
};
Convert.FromDate = {
    /**
     * Converts from Date to time in milliseconds.
     */
    ToTimeInMillis: new ConvertFromDate()
};
/**
 * Converts from time in milliseconds to Date.
 */
Convert.ToDate = {
    FromTimeInMillis: new ConvertToDate()
};
