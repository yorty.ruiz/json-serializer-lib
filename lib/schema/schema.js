"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Schema = void 0;
const commons_1 = require("../commons");
const config_1 = require("../config");
const func_1 = require("../func");
const enums_1 = require("./enums");
/**
 * Utilities for schema.
 */
class Schema {
    static parseString(str) {
        const parsed = Schema.TYPE_DESCRIPTOR_REG_EXP.exec(str);
        let r = [enums_1.TypeDescriptor.NONE.toString(), str];
        if (parsed && parsed.length == 3) {
            for (const key in enums_1.TypeDescriptor) {
                if (enums_1.TypeDescriptor[key] === parsed[1]) {
                    r = [enums_1.TypeDescriptor[key], parsed[2]];
                    break;
                }
            }
        }
        commons_1.Logger.instance.debug(() => `\nparseString('${str}') -> [${r.map(el => "'" + el + "'").join(', ')}]\n`);
        return r;
    }
    /**
     * Inverts the given schema.
     *
     * @param schema the input schema.
     * @returns the inverted schema.
     */
    static invert(schema) {
        const invertedSchema = {};
        for (const property in schema) {
            const value = schema[property];
            const [typeDescriptor, newValue] = Schema.parseString(property);
            const inputType = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(value);
            switch (inputType) {
                case func_1.InputType.NATIVE:
                    invertedSchema[typeDescriptor + value] = newValue;
                    break;
                case func_1.InputType.OBJECT:
                    {
                        const invSch = {};
                        invSch[enums_1.Key.PROP_NAME] = newValue;
                        const objDescriptor = value;
                        if (enums_1.Key.DEFAULT_VALUE in objDescriptor) {
                            invSch[enums_1.Key.DEFAULT_VALUE] = objDescriptor[enums_1.Key.DEFAULT_VALUE];
                        }
                        if (enums_1.Key.KEEP_DEFAULT_VALUE in objDescriptor) {
                            invSch[enums_1.Key.KEEP_DEFAULT_VALUE] = objDescriptor[enums_1.Key.KEEP_DEFAULT_VALUE];
                        }
                        if (enums_1.Key.CONVERTER in objDescriptor) {
                            // NOTE: the _serFunc and _desFunc are not interchanged, on purpose.
                            invSch[enums_1.Key.CONVERTER] = objDescriptor[enums_1.Key.CONVERTER];
                        }
                        if (enums_1.Key.SCHEMA in objDescriptor) {
                            invSch[enums_1.Key.SCHEMA] = Schema.invert(objDescriptor[enums_1.Key.SCHEMA]);
                        }
                        invertedSchema[typeDescriptor + objDescriptor[enums_1.Key.PROP_NAME]] = invSch;
                    }
                    break;
                default:
                    commons_1.Logger.instance.warn(() => `${inputType} is not valid to invert a schema.`);
                    break;
            }
        }
        return invertedSchema;
    }
    /**
     * Merges one schema into another schema.
     *
     * @param src source schema.
     * @param dst destination schema.
     */
    static merge(src, dst) {
        for (const property in src) {
            if (property in dst && func_1.FunctionUtil.TYPE_CLASSIFIER.apply(src[property]) === func_1.InputType.OBJECT) {
                Schema.merge(src[property], dst[property]);
            }
            else {
                dst[property] = src[property];
            }
        }
    }
    static getTypeDescriptor(str) {
        const [typeDescriptorStr,] = Schema.parseString(str);
        switch (typeDescriptorStr || str) {
            case enums_1.TypeDescriptor.OBJECT.toString():
                return enums_1.TypeDescriptor.OBJECT;
            case enums_1.TypeDescriptor.ARRAY.toString():
                return enums_1.TypeDescriptor.ARRAY;
            case enums_1.TypeDescriptor.STRING.toString():
                return enums_1.TypeDescriptor.STRING;
            case enums_1.TypeDescriptor.NUMBER.toString():
                return enums_1.TypeDescriptor.NUMBER;
            case enums_1.TypeDescriptor.BOOLEAN.toString():
                return enums_1.TypeDescriptor.BOOLEAN;
            case enums_1.TypeDescriptor.DATE.toString():
                return enums_1.TypeDescriptor.DATE;
            default:
                return enums_1.TypeDescriptor.NONE;
        }
    }
    static getDefaultValueByType(typeDescriptor) {
        switch (typeDescriptor) {
            case enums_1.TypeDescriptor.OBJECT:
                return {};
            case enums_1.TypeDescriptor.ARRAY:
                return [];
            case enums_1.TypeDescriptor.STRING:
                return config_1.ConfigurationUtil.DEFAULTS.STRING;
            case enums_1.TypeDescriptor.NUMBER:
                return config_1.ConfigurationUtil.DEFAULTS.NUMBER;
            case enums_1.TypeDescriptor.BOOLEAN:
                return config_1.ConfigurationUtil.DEFAULTS.BOOLEAN;
            default:
                return null;
        }
    }
    static hasDefaultValue(schema) {
        return schema && enums_1.Key.DEFAULT_VALUE in schema;
    }
    static getDefaultValue(schema) {
        return Schema.hasDefaultValue(schema) ? schema[enums_1.Key.DEFAULT_VALUE] : null;
    }
    static getConverterFunction(schema, functionKey) {
        if (func_1.FunctionUtil.TYPE_CLASSIFIER.apply(schema) !== func_1.InputType.OBJECT) {
            return null;
        }
        const objDescriptor = schema;
        if (!(enums_1.Key.CONVERTER in objDescriptor)) {
            return null;
        }
        const converter = objDescriptor[enums_1.Key.CONVERTER];
        if (!(functionKey in converter)) {
            return null;
        }
        return {
            apply: converter[functionKey]
        };
    }
    static printPath(schema) {
        let path = schema.propInObj;
        let parent = schema.parent;
        while (parent !== null) {
            path = parent.propInObj + '.' + path;
            parent = parent.parent;
        }
        return path;
    }
    static getSchema(jsonSch) {
        const schema = {
            parent: null,
            typeDesc: enums_1.TypeDescriptor.OBJECT,
            propInObj: '$',
            propInSch: '',
            propNewName: '',
            map: new Map(),
            hasDefVal: false,
            defVal: null,
            keepDef: false
        };
        Schema.buildSchema(jsonSch, schema);
        return schema;
    }
    static buildSchema(object, schema) {
        for (const key in object) {
            if (!Object.prototype.hasOwnProperty.call(object, key)) {
                continue;
            }
            const element = object[key];
            const typeOfElement = func_1.FunctionUtil.TYPE_CLASSIFIER.apply(element);
            if (typeOfElement === func_1.InputType.NATIVE) {
                Schema.putNativePropSchema(element, key, schema);
            }
            else if (typeOfElement === func_1.InputType.OBJECT) {
                const objDescriptor = element;
                if (enums_1.Key.SCHEMA in objDescriptor) {
                    const objectItem = Schema.putObjectPropSchema(objDescriptor, key, schema);
                    Schema.buildSchema(objDescriptor[enums_1.Key.SCHEMA], objectItem);
                }
                else {
                    Schema.putNativePropSchema(objDescriptor, key, schema);
                }
            }
            else {
                commons_1.Logger.instance.warn(() => `${typeOfElement} is not valid to genetare a schema.`);
            }
        }
    }
    static putNativePropSchema(element, key, schema) {
        let serFn;
        let desFn;
        let newName;
        let keepDef;
        let hasDefVal;
        let defVal;
        const [typeDescriptorStr, propertyName] = Schema.parseString(key);
        if (typeof element === 'string') {
            newName = element;
            serFn = null;
            desFn = null;
            keepDef = false;
            hasDefVal = false;
            defVal = null;
            // Inherite.
            if (schema.hasDefVal && func_1.FunctionUtil.TYPE_CLASSIFIER.apply(schema.defVal) === func_1.InputType.OBJECT) {
                const objDefVal = schema.defVal;
                if (propertyName in objDefVal) {
                    defVal = objDefVal[propertyName];
                }
                else if (newName in objDefVal) {
                    defVal = objDefVal[newName];
                }
            }
        }
        else {
            newName = element[enums_1.Key.PROP_NAME];
            serFn = Schema.getConverterFunction(element, enums_1.Key.SERIALIZE_FUNCTION);
            desFn = Schema.getConverterFunction(element, enums_1.Key.DESERIALIZE_FUNCTION);
            keepDef = element[enums_1.Key.KEEP_DEFAULT_VALUE] === true;
            hasDefVal = Schema.hasDefaultValue(element);
            defVal = Schema.getDefaultValue(element);
        }
        schema.map.set(propertyName, {
            parent: schema,
            typeDesc: Schema.getTypeDescriptor(typeDescriptorStr),
            propInSch: key,
            propInObj: propertyName,
            propNewName: newName,
            hasDefVal: schema.hasDefVal || hasDefVal,
            defVal: defVal,
            keepDef: schema.keepDef || keepDef,
            hasConv: serFn !== null || desFn !== null,
            serFn: serFn,
            desFn: desFn
        });
    }
    static putObjectPropSchema(element, key, schema) {
        const [typeDescriptorStr, propertyName] = Schema.parseString(key);
        const item = {
            parent: schema,
            typeDesc: Schema.getTypeDescriptor(typeDescriptorStr),
            propInSch: key,
            propInObj: propertyName,
            propNewName: element[enums_1.Key.PROP_NAME],
            map: new Map(),
            hasDefVal: Schema.hasDefaultValue(element),
            defVal: Schema.getDefaultValue(element),
            keepDef: element[enums_1.Key.KEEP_DEFAULT_VALUE] === true
        };
        schema.map.set(propertyName, item);
        return item;
    }
}
exports.Schema = Schema;
/**
 * Group 1: type descriptor
 * Group 2: property name.
 */
Schema.TYPE_DESCRIPTOR_REG_EXP = /^(t@(?:a|b|o|n|s|d)_)(.+)/;
