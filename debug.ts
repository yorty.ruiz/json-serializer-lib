import {Model} from './src/serializer'

import { INPUT } from "./spec/demos/1/input";
import { SCHEMA } from "./spec/demos/1/schema";
import {FunctionUtil} from "./src/func";
import {GeneratorUtil} from './src/schema/generator';

const print = (obj:unknown):void => {
  console.log(JSON.stringify(obj, null, 2).replace(/"/g, "'"));
};

//print(INPUT);
const sch = GeneratorUtil.DEFAULT.apply(INPUT);
//print(SCHEMA);
console.log(FunctionUtil.DEEP_EQUALS.apply(SCHEMA, sch));

const serR = Model.serialize(INPUT, SCHEMA).result;
//print(serR);
const desR = Model.deserialize(serR, SCHEMA).result;

console.log(FunctionUtil.DEEP_EQUALS.apply(INPUT, desR));
print(desR);
