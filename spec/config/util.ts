import {IConfig, StrategyForUnknownProperty} from '../../src/config';
import {ISerializationSettingsBuilder, IDeserializationSettingsBuilder} from '../../src/config/settingsBuilder';

export const checkDefinedMethods = (builder: ISerializationSettingsBuilder | IDeserializationSettingsBuilder) => {

  it('Should be defined.', () => {
    expect(builder).toBeDefined();
  });

  it('method "includeDefaultValues" should be defined.', () => {
    expect(builder.includeDefaultValues).toBeDefined();
  });

  it('method "includeEmptyArrays" should be defined.', () => {
    expect(builder.includeEmptyArrays).toBeDefined();
  });

  it('method "includeEmptyObjects" should be defined.', () => {
    expect(builder.includeEmptyObjects).toBeDefined();
  });

  it('method "includeDefaultBoolean" should be defined.', () => {
    expect(builder.includeDefaultBoolean).toBeDefined();
  });

  it('method "includeUndefinedValues" should be defined.', () => {
      expect(builder.includeUndefinedValues).toBeDefined();
  });

  it('method "includeDefaultNumber" should be defined.', () => {
     expect(builder.includeDefaultNumber).toBeDefined();
  });

  it('method "includeDefaultString" should be defined.', () => {
       expect(builder.includeDefaultString).toBeDefined();
  });

  it('method "includeUserDefinedDefaultValues" should be defined.', () => {
     expect(builder.includeUserDefinedDefaultValues).toBeDefined();
  });

  it('method "strategyForUnknownProperty" should be defined.', () => {
      expect(builder.strategyForUnknownProperty).toBeDefined();
  });

  it('method "setBooleanConverter" should be defined.', () => {
      expect(builder.setBooleanConverter).toBeDefined();
  });

  it('method "setDateConverter" should be defined.', () => {
      expect(builder.setDateConverter).toBeDefined();
  });

  it('method "includeStats" should be defined.', () => {
    expect(builder.includeStats).toBeDefined();
  });

  it('method "build" should be defined.', () => {
    expect(builder.build).toBeDefined();
  });
};

export const checkDefaultConfig = (config: IConfig, value: boolean, s4up: StrategyForUnknownProperty) => {

  it('"includeDefaultValues" should be: ' + value + '.',() => {
    expect(config.includeDefaultValues).toBe(value);
  });

  it('"includeEmptyArrays" should be: ' + value + '.',() => {
    expect(config.includeEmptyArrays).toBe(value);
  });

  it('"includeEmptyObjects" should be: ' + value + '.',() => {
    expect(config.includeEmptyObjects).toBe(value);
  });

  it('"includeNullValues" should be: ' + value + '.',() => {
    expect(config.includeNullValues).toBe(value);
  });

  it('"includeUndefinedValues" should be: ' + value + '.',() => {
    expect(config.includeUndefinedValues).toBe(value);
  });

  it('"includeDefaultBoolean" should be: ' + value + '.',() => {
    expect(config.includeDefaultBoolean).toBe(value);
  });

  it('"includeDefaultString" should be: ' + value + '.',() => {
    expect(config.includeDefaultString).toBe(value);
  });

  it('"includeDefaultNumber" should be: ' + value + '.',() => {
    expect(config.includeDefaultNumber).toBe(value);
  });

  it('"strategyForUnknownProperty" should be: ' + s4up + '.',() => {
    expect(config.strategyForUnknownProperty).toBe(s4up);
  });

  it('"includeStats" should be: false.',() => {
    expect(config.includeStats).toBeFalse();
  });
}

