import { Model } from '../../../src/serializer';
import {
  ConfigurationUtil as ConfUtil,
  StrategyForUnknownProperty as S4UnkProp
} from '../../../src/config';
import { SER_OBJECT, SCHEMA } from './data';


describe('Model.deserialize :::: Unknown Properties.', () => {


  it('Default configuration.', () => {
    expect(
      Model.deserialize(
        SER_OBJECT,
        SCHEMA
      ).deserializedJSON
    )
    .toEqual(
      {
        property_01: 1,                     // Property defined in the schema.
        property_02: 'OK...',               // Property defined in the schema.
        property_03: true,                  // Property defined in the schema.
        property_04: [ 2, 4, 6 ],           // Property defined in the schema.
        property_05: {                      // Property defined in the schema.
          subProperty01: 'OK... OK... :)'
        },
        unknown_prop_01: 100,
        unknown_prop_02: 'Hello WORLD!!!',
        unknown_prop_03: true,
        unknown_prop_04: [ 1, 3, 5 ],
        unknown_prop_05: {
          subProperty01: 'Happy coding!!!'
        }

      });
  });


  it('Explicit configuration.', () => {
    expect(
      Model.deserialize(
        SER_OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.DESERIALIZER
        .strategyForUnknownProperty(S4UnkProp.KEEP)
        .build()
      ).deserializedJSON
    )
    .toEqual(
      {
        property_01: 1,                     // Property defined in the schema.
        property_02: 'OK...',               // Property defined in the schema.
        property_03: true,                  // Property defined in the schema.
        property_04: [ 2, 4, 6 ],           // Property defined in the schema.
        property_05: {                      // Property defined in the schema.
          subProperty01: 'OK... OK... :)'
        },
        unknown_prop_01: 100,
        unknown_prop_02: 'Hello WORLD!!!',
        unknown_prop_03: true,
        unknown_prop_04: [ 1, 3, 5 ],
        unknown_prop_05: {
          subProperty01: 'Happy coding!!!'
        }

      });
  });

});
