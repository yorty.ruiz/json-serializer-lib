import { Model } from '../../../src/serializer';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';

describe('Model.serialize :::: Default config.', () => {

  it('', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
      ).serializedJSON
    )
    .toEqual(
      {
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});
