import { Model } from '../../../src/serializer';
import { ConfigurationUtil as ConfUtil } from '../../../src/config';
import { DATE_VALUE, OBJECT, SCHEMA } from './data';



describe('Model.serialize :::: Include: ', () => {

  it('Zero values.', () => {
    expect(
      Model.serialize(
        OBJECT,
        SCHEMA,
        ConfUtil.settingsBuilderFor.SERIALIZER // Default config.
        .includeDefaultNumber(true)   // keep all zero values.
        .build()
      ).serializedJSON
    )
    .toEqual(
      {
        ppv01: 0, // <-- Zero will be included.
        ppv08: {
          sp01: 0 // <-- Zero will be included.
        },
        p01: Math.PI,
        p02: 'Hello WORLD!!!',
        p03: true,
        p04: [ 1, 2, 3 ],
        p05: {
          sp01: 'Happy coding',
          sp02: DATE_VALUE
        }
      }
    );
  });

});
