import { Convert } from '../../src/func/converters';

describe('Functions - Convert From Boolean To: ', () => {

  it('0 Or 1 when true should be 1.', () => {
    expect(Convert.FromBoolean.To_0_Or_1.apply(true)).toEqual(1);
  });
  it('0 Or 1 when false should be 0.', () => {
    expect(Convert.FromBoolean.To_0_Or_1.apply(false)).toEqual(0);
  });


  it('"F" Or "T" when false should be "F".', () => {
    expect(Convert.FromBoolean.To_F_Or_T.apply(false)).toEqual('F');
  });
  it('"F" Or "T" when true should be "T".', () => {
    expect(Convert.FromBoolean.To_F_Or_T.apply(true)).toEqual('T');
  });


  it('"N" Or "Y" when true should be "Y".', () => {
    expect(Convert.FromBoolean.To_N_Or_Y.apply(true)).toEqual('Y');
  });
  it('"N" Or "Y" when false should be "N".', () => {
    expect(Convert.FromBoolean.To_N_Or_Y.apply(false)).toEqual('N');
  });


  it('To Custom validation error.', () => {
    expect(() => Convert.FromBoolean.ToCustom({})).toThrowError();
  });

  it('To Custom. 1', () => {
    let functObj = Convert.FromBoolean.ToCustom({
      true: 'A',
      false: 'B'
    });
    expect(functObj.apply(true)).toEqual('A');
    expect(functObj.apply(false)).toEqual('B');
  });

  it('To Custom. 2', () => {
    let functObj = Convert.FromBoolean.ToCustom({
      true: 1,
      false: -1
    });
    expect(functObj.apply(true)).toEqual(1);
    expect(functObj.apply(false)).toEqual(-1);
  });


});

