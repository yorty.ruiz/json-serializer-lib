import {Convert} from "../../../src/func/converters";
import {INativePropSchema, IObjectPropSchema, Schema} from "../../../src/schema";
import {TypeDescriptor} from "../../../src/schema/enums";


describe('Schema.getSchema :::: ', () => {




  const JSON_SCHEMA = {
    't@o_job': {
      _propName: 'j',
      _sch: {
        't@n_worked_years': 'wy',
        't@s_title': 't'
      },
      // Define the default value as an entire object.
      _defVal: {
        title: 'UNKNOWN',
        worked_years: -1
      }
    },

  };

  const SCHEMA: IObjectPropSchema = Schema.getSchema(JSON_SCHEMA);

  it('Inherite default values from the object.', () => {
    const JOB_SCHEMA: IObjectPropSchema = (SCHEMA.map.get('job') as IObjectPropSchema);
    expect(JOB_SCHEMA).toBeDefined();
    expect(JOB_SCHEMA.hasDefVal).toBeTrue();
    expect(JOB_SCHEMA.defVal).toEqual(JSON_SCHEMA['t@o_job']._defVal);
    const NATIVE_SCHEMA_CHILD_TITLE = (JOB_SCHEMA.map.get('title') as INativePropSchema);
    const NATIVE_SCHEMA_CHILD_WORKED_YEARS = (JOB_SCHEMA.map.get('worked_years') as INativePropSchema);
    expect(NATIVE_SCHEMA_CHILD_TITLE.keepDef).toBe(JOB_SCHEMA.keepDef);
    expect(NATIVE_SCHEMA_CHILD_TITLE.keepDef).toBe(JOB_SCHEMA.keepDef);
    expect(NATIVE_SCHEMA_CHILD_WORKED_YEARS.hasDefVal).toBeTrue();
    expect(NATIVE_SCHEMA_CHILD_WORKED_YEARS.hasDefVal).toBeTrue();
    expect(NATIVE_SCHEMA_CHILD_TITLE.defVal).toBe(JSON_SCHEMA['t@o_job']._defVal.title);
    expect(NATIVE_SCHEMA_CHILD_WORKED_YEARS.defVal).toBe(JSON_SCHEMA['t@o_job']._defVal.worked_years);
  });


});
