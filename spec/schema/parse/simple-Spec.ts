import {Logger, LogLevel} from "../../../src/commons";
import {Schema} from "../../../src/schema";


describe('Schema.search :::: Simple: ', () => {

  beforeAll(() => {
    Logger.setLevel(LogLevel.DEBUG);
  });

  afterAll(() => {
    Logger.setLevel(LogLevel.NONE);
  });

  it('No type descriptor.', () => {

    const [typeDescriptor, propertyName] = Schema.parseString('property01');
    expect(typeDescriptor).toEqual('');
    expect(propertyName).toEqual('property01');

  });

  it('Type descriptor.', () => {

    const [typeDescriptor, propertyName] = Schema.parseString('t@n_property02');
    expect(typeDescriptor).toEqual('t@n_');
    expect(propertyName).toEqual('property02');

  });


});
