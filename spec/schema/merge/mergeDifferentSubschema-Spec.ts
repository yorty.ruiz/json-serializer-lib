import {Schema} from "../../../src/schema";

describe('Schema.merge :::: Subschema: ', () => {

  it('No common properties.', () => {
    let sch1: any = {
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: 'p12'
        }
      }
    };
    let sch2: any = {
      prop_a: 'pa',
      prop_b: {
        _propName: 'pb',
        _sch: {
          prop_1_a: 'p1a',
          prop_1_b: 'p1b'
        }
      }
    };
    Schema.merge(sch1, sch2);
    expect(sch2).toEqual({
      // From sch1
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: 'p12'
        }
      },
      // From sch2
      prop_a: 'pa',
      prop_b: {
        _propName: 'pb',
        _sch: {
          prop_1_a: 'p1a',
          prop_1_b: 'p1b'
        }
      }
    });
  });

});
