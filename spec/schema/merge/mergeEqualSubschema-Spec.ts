import {Schema} from "../../../src/schema";

describe('Schema.merge :::: Subschema: ', () => {


  it('Equal schemas.', () => {

    let sch1: any = {
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: 'p12'
        }
      }
    };

    let sch2: any = {
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: 'p12'
        }
      }
    };

    Schema.merge(sch1, sch2);

    expect(sch2).toEqual({
      prop_1: 'p1',
      prop_2: {
        _propName: 'p2',
        _sch: {
          prop_1_1: 'p11',
          prop_1_2: 'p12'
        }
      }
    });
  });




});
