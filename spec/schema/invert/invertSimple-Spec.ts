import { Schema } from '../../../src/schema';



describe('Schema.invert :::: ', () => {


  it('Simple.', () => {

    expect(Schema.invert({
      property1: 'p1',
      property2: 'p2',
      property3: 'p3',
      property4: 'p4',
      property5: 'p5'
    }))
    .toEqual({
      p1: 'property1',
      p2: 'property2',
      p3: 'property3',
      p4: 'property4',
      p5: 'property5'
    });

  });

});
