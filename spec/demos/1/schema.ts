/**
 * Schema
 */

export const SCHEMA = {
  't@a_addresses': {
    '_propName': 'a',
    '_sch': {
      't@b_is_home': 'ih',
      't@n_zip_code': 'zc',
      't@s_city': 'c',
      't@s_country': 'c1',
      't@s_state': 's',
      't@s_street': 's2'
    }
  },
  't@a_purchases': {
    '_propName': 'p',
    '_sch': {
      't@n_product_id': 'pi',
      't@n_quantity': 'q',
      't@s_product_description': 'pd'
    }
  },
  't@b_is_a_male': 'iam',
  't@n_amount_of_purchases': 'aop',
  't@n_heigh_cm': 'hc',
  't@n_weigth_kg': 'wk',
  't@o_job': {
    '_propName': 'j',
    '_sch': {
      't@n_worked_years': 'wy',
      't@s_title': 't'
    }
  },
  't@s_birth_date': 'bd',
  't@s_first_name': 'fn',
  't@s_last_name': 'ln'
};
