import {Model} from '../../../src/serializer'


////////////////////////////////////////////////////
// Opt 1
const OBJ_JOB_SCH_1 = {
  't@n_worked_years': {
    _propName: 'wy',
    // Define the custom value to -1
    _defVal: -1
  },
  't@s_title': {
    _propName: 't',
    // Define the custom value to 'UNKNOWN'
    _defVal: 'UNKNOWN'
  }
};

const OBJ_PERSON_SCH_1 = {

  't@o_job': {
    _propName: 'j',
    _sch: OBJ_JOB_SCH_1
  },

};

////////////////////////////////////////////////////
// Opt 2
const OBJ_JOB_SCH_2 = {
  't@n_worked_years': 'wy',
  't@s_title': 't'
};


const OBJ_PERSON_SCH_2 = {

  't@o_job': {
    _propName: 'j',
    _sch: OBJ_JOB_SCH_2,
    // Define the default value as an entire object.
    _defVal: {
      title: 'UNKNOWN',
      worked_years: -1
    }
  },

};
////////////////////////////////////////////////////
// Opt 3
const OBJ_JOB_SCH_3 = {
  't@n_worked_years': {
    _propName: 'wy',
    // Define the custom value to null
    _defVal: null 
  },
  't@s_title': 't'
};

const OBJ_PERSON_SCH_3 = {

  't@o_job': {
    _propName: 'j',
    _sch: OBJ_JOB_SCH_3
  },

};
////////////////////////////////////////////////////
// Opt 4
const OBJ_JOB_SCH_4 = {
  'worked_years': {
    _propName: 'wy',
    _keepDef: true // includes any default value.
  },
  't@s_title': 't'
};

const OBJ_PERSON_SCH_4 = {

  't@o_job': {
    _propName: 'j',
    _sch: OBJ_JOB_SCH_4
  },

};


/////////////////////////////////////////////////////
// Person
const OBJ_1 = {
  job: {
    // Does not match the default
    worked_years: 10,
    // Does not match the default
    title: 'Engineer'
  }
};

const OBJ_2 = {
  job: {
    // Matches the default defined in the schema
    worked_years: -1,
    // Does not match the default
    title: 'Engineer'
  }
};


const OBJ_3 = {
  job: {
    // Matches the default defined in the schema
    worked_years: -1,
    // Matches the default defined in the schema
    title: 'UNKNOWN'
  }
};


const OBJ_4 = {
  job: {
    // Does not match the default
    worked_years: 10,
    // Default value in the schema
    title: 'UNKNOWN'
  }
};


const OBJ_5 = {
  job: {
    // Does not match the default, but null is also a predefined default value
    worked_years: null,
    // Does not match the default
    title: 'Engineer'
  }
};

const OBJ_6 = {
  job: {
    worked_years: 0,
    title: 'Engineer'
  }
};


const apply = (obj: any, sch: any): [any, any] => {
  const serializedJSON = Model.serialize(obj, sch).serializedJSON;
  const deserializedJSON = Model.deserialize(serializedJSON, sch).deserializedJSON;
  return [ serializedJSON, deserializedJSON ];
};


describe('Demo 6', () => {

  it('1', () => {

    const [s1, d1] = apply(OBJ_1, OBJ_PERSON_SCH_1);
    const [s2, d2] = apply(OBJ_1, OBJ_PERSON_SCH_2);


    const r = {
      "j": {
        "wy": 10,
        "t": "Engineer"
      }
    };

    expect(s1).toEqual(r);
    expect(s2).toEqual(r);
    expect(d1).toEqual(OBJ_1);
    expect(d2).toEqual(OBJ_1);

  });

  it('2', () => {

    const [s1, d1] = apply(OBJ_2, OBJ_PERSON_SCH_1);
    const [s2, d2] = apply(OBJ_2, OBJ_PERSON_SCH_2);

    const r = {
      "j": {
        "t": "Engineer"
      }
    };

    expect(s1).toEqual(r);
    expect(s2).toEqual(r);
    expect(d1).toEqual(OBJ_2);
    expect(d2).toEqual(OBJ_2);

  });

  it('3', () => {

    const [s1, d1] = apply(OBJ_3, OBJ_PERSON_SCH_1);
    const [s2, d2] = apply(OBJ_3, OBJ_PERSON_SCH_2);


    expect(s1).toEqual({});
    expect(s2).toEqual({});
    expect(d1).toEqual(OBJ_3);
    expect(d2).toEqual(OBJ_3);

  });


  it('4', () => {

    const [s1, d1] = apply(OBJ_4, OBJ_PERSON_SCH_1);
    const [s2, d2] = apply(OBJ_4, OBJ_PERSON_SCH_2);

    const r = {
      "j": {
        "wy": 10
      }
    };

    expect(s1).toEqual(r);
    expect(s2).toEqual(r);
    expect(d1).toEqual(OBJ_4);
    expect(d2).toEqual(OBJ_4);

  });

  it('5', () => {

    const [s1, d1] = apply(OBJ_5, OBJ_PERSON_SCH_1);
    const [s2, d2] = apply(OBJ_5, OBJ_PERSON_SCH_2);
    const [s3, d3] = apply(OBJ_5, OBJ_PERSON_SCH_3);

    const r = {
      "j": {
        "t": "Engineer"
      }
    };

    expect(s1).toEqual(r);
    expect(s2).toEqual(r);
    expect(s3).toEqual(r);
    expect(d1).not.toEqual(OBJ_5);
    expect(d2).not.toEqual(OBJ_5);
    expect(d3).toEqual(OBJ_5);
  });

  it('6', () => {

    const [s1, d1] = apply(OBJ_5, OBJ_PERSON_SCH_4);
    const [s2, d2] = apply(OBJ_6, OBJ_PERSON_SCH_4);

    const r1 = {
      "j": {
        "wy": null, // includes null
        "t": "Engineer"
      }
    };
    const r2 = {
      "j": {
        "wy": 0, //  also includes 0
        "t": "Engineer",
      }
    };

    expect(s1).toEqual(r1);
    expect(s2).toEqual(r2);
    expect(d1).toEqual(OBJ_5);
    expect(d2).toEqual(OBJ_6);
  });

});
